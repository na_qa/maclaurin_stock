from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Strategy)
admin.site.register(StrategyCost)
admin.site.register(Staff)
admin.site.register(Contract)