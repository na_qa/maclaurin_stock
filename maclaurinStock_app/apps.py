from django.apps import AppConfig


class MaclaurinstockAppConfig(AppConfig):
    name = 'maclaurinStock_app'
