# Generated by Django 3.0.7 on 2020-06-14 07:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('maclaurinStock_setting', '0002_auto_20200614_1621'),
        ('maclaurinStock_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='strategy',
            name='name',
            field=models.ForeignKey(db_column='name', on_delete=django.db.models.deletion.CASCADE, to='maclaurinStock_setting.StrategyNameGroup', to_field='strategy_name'),
        ),
        migrations.AlterField(
            model_name='strategycost',
            name='group',
            field=models.ForeignKey(db_column='group', on_delete=django.db.models.deletion.CASCADE, to='maclaurinStock_setting.StrategyCostGroup', to_field='strategy_cost'),
        ),
        migrations.AlterField(
            model_name='strategycost',
            name='name',
            field=models.ForeignKey(db_column='name', on_delete=django.db.models.deletion.CASCADE, to='maclaurinStock_setting.StrategyNameGroup', to_field='strategy_name'),
        ),
    ]
