# Generated by Django 3.0.7 on 2020-07-10 10:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maclaurinStock_app', '0011_auto_20200615_1729'),
    ]

    operations = [
        migrations.AddField(
            model_name='strategy',
            name='strategy_detail_image',
            field=models.ImageField(blank=True, null=True, upload_to='%Y/%m/%d'),
        ),
    ]
