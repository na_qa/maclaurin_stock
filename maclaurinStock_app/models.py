from maclaurinStock_setting.models import *

# Create your models here.
class Strategy(CommonInfo):
    name = models.ForeignKey(StrategyNameGroup, to_field='strategy_name', db_column='name', on_delete=models.CASCADE)
    description = models.CharField(max_length=max_length)
    profit_rate = models.DecimalField(decimal_places=decimal_places, max_digits=max_digits)
    from_date = models.DateField()
    to_date = models.DateField()
    strategy_image = models.ImageField(upload_to=f'%Y/%m/%d', blank=True, null=True)
    strategy_detail_image = models.ImageField(upload_to=f'%Y/%m/%d', blank=True, null=True)
    stock_image = models.ImageField(upload_to=f'%Y/%m/%d', blank=True, null=True)

class StrategyCost(CommonInfo):
    name = models.ForeignKey(StrategyNameGroup, to_field='strategy_name', db_column='name',  on_delete=models.CASCADE)
    group = models.ForeignKey(StrategyCostGroup, to_field='strategy_cost', db_column='group', on_delete=models.CASCADE)
    cost = models.BigIntegerField()

class Staff(CommonInfo):
    name = models.CharField(unique=True, max_length=max_length)
    description = models.TextField()
    image = models.ImageField(upload_to=f'%Y/%m/%d', blank=True, null=True)

class Contract(CommonInfo):
    name = models.CharField(max_length=max_length)
    contract_num = models.TextField()
