from django.shortcuts import render
from django.db.models import *
from .models import *
from django.template.response import TemplateResponse
import functools
from django.shortcuts import reverse
import datetime
from django.http import JsonResponse
# Create your views here.
def navsDecorator(function):
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        getNav = lambda url, name, *args, **kwargs:{"url":reverse(url, kwargs=kwargs), "name": name, 'url_name':url, 'kwargs':kwargs}
        template = function(*args, **kwargs)
        try:
            navs = [
                getNav('main','상품 소개'),
                getNav('about','서비스 소개'),
                getNav('staff','멤버'),
            ]
            template.context_data['navs'] = navs
            return template.render()
        except Exception as e:
            print(e)
            return template
    return wrapper

@navsDecorator
def main(req):
    if req.method == 'GET':
        sections = Strategy.objects.filter(status=0).values()
        for section in sections:
            costs = StrategyCost.objects.filter(status=0).values('cost', 'group').filter(name = section['name_id'])
            section['costs'] = []
            for cost in costs:
                section['costs'].append({'cost':cost['cost'], 'group':cost['group']})
            section['total_date'] = section['to_date'] - section['from_date']
        return TemplateResponse(req, template='main.html', context={'sections': sections}, status=302)
    elif req.method == 'POST':
        try:
            data = req.POST.copy()
            data = {key:val for key, val in data.items()}
            Contract(**data).save()
            res = JsonResponse({'msg': "제출 되었습니다\n연락 드리겠습니다"})
            res.status_code = 200
            return res
        except:
            res = JsonResponse({'msg': f"입력실패", 'detail': f"입력실패"})
            res.status_code = 400
            return res


@navsDecorator
def about(req):
    return TemplateResponse(req, template='about.html', context={}, status=302)

@navsDecorator
def staff(req):
    sections = Staff.objects.filter(status=0).values()
    return TemplateResponse(req, template='staff.html', context={'sections': sections}, status=302)

def ssl(req, id):
    return TemplateResponse(req, template='ssl', context={})