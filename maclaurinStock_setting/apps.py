from django.apps import AppConfig


class MaclaurinstockSettingConfig(AppConfig):
    name = 'maclaurinStock_setting'
