from django.db import models
from django.contrib.auth.models import AbstractUser, PermissionsMixin, AbstractBaseUser
from django.urls import reverse
from django.utils import timezone

# Create your models here.
decimal_places = 2
max_digits = 20
max_length = 255
ignore_field = set(["created_by", "updated_at", "updated_by", "created_at", "status"])

def makeModelString(self):
    field_values = []
    for field in self._meta.get_fields():
        try:
            field_name = field.attname
            if field_name in ignore_field:
                continue
            field_values.append(str(self.__getattribute__(field.attname)))
        except:
            pass
    return '-'.join(field_values)


class CommonInfo(models.Model):
    created_by = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_by = models.TextField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    STATUS_CHOICE = (
        # (CODE, "DESCRIPTION"),
        (2, "Update"),
        (1, "Pending Updated"),
        (0, "Done"),
        (-1, "Pending Deleted"),
        (-2, "Delete"),
    )
    status = models.IntegerField(choices=STATUS_CHOICE, default=0)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(CommonInfo, self).save(*args, **kwargs)

    def __str__(self):
        return makeModelString(self)

    class Meta:
        abstract = True

class StrategyNameGroup(models.Model):
    strategy_name = models.CharField(primary_key=True, unique=True, max_length=max_length)

class StrategyCostGroup(models.Model):
    strategy_cost = models.CharField(primary_key=True, unique=True, max_length=max_length)
